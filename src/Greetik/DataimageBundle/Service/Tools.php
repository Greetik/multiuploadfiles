<?php

namespace Greetik\DataimageBundle\Service;

use Greetik\DataimageBundle\Entity\Dataimage;
use Symfony\Component\HttpFoundation\Response;
use UploadHandler;
use finfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */
class Tools {

    private $em;
    private $rootdir;
    private $router;
    private $container;
    private $interfacetools;
    private $path;
    private $changefilename;
    private $sftp_login;
    private $ftp_data ='';
    private $forbiddenupload=false;
    private $dataimagetypechoices='';
    private $dataimagetypechoicesfunction='';
    
    public function __construct($_entityManager, $_rootdir, $_router, $_token, $_container, $_interfacetools, $_path, $_changefilename = false, $_ftp_data='', $_dataimagetypechoices='', $_dataimagetypefunction='') {
        $this->em = $_entityManager;
        $this->rootdir = $_rootdir;
        $this->router = $_router;
        $this->container = $_container;
        $this->interfacetools = $_interfacetools;
        $this->dataimagetypechoices = $_dataimagetypechoices;
        $this->dataimagetypechoicesfunction = $_dataimagetypefunction;

        if (!empty($_ftp_data)){
            $this->ftp_data =  unserialize(base64_decode($_ftp_data));
        }
        else
            $this->ftp_data = '';
            
        //put the bars
        if ($_path[0] != '/')
            $_path = '/' . $_path;
        if ($_path[strlen($_path) - 1] != '/')
            $_path .= '/';
        $this->path = $_path;

        //create dir if not exists
        $auxpath = explode('/', $this->path);
        $path = '';
        foreach ($auxpath as $folder) {
            $path .= $folder . '/';
            if (!file_exists($this->rootdir . '/../' . $path))
                mkdir($this->rootdir . '/../' . $path);
        }

        $this->changefilename = $_changefilename;
    }

    public function dataConnexions($itemtype){
        switch($this->ftp_data['ftp_type']){
            case 0: return array(
                'ftp_host'=>$_token->getToken()->getUser()->getFtphost(),
                'ftp_user' => $_token->getToken()->getUser()->getFtpuser(),
                'ftp_pwd' => $_token->getToken()->getUser()->getFtppwd(),
                'ftp_port' => $_token->getToken()->getUser()->getFtpport(),
                'ftp_path' => $_token->getToken()->getUser()->getFtpport()
            );
            case 1: return array(
                'ftp_host'=>$this->ftp_data['ftp_host'],
                'ftp_user' => $this->ftp_data['ftp_user'],
                'ftp_pwd' => $this->ftp_data['ftp_pwd'],
                'ftp_port' => $this->ftp_data['ftp_port'],
                'ftp_path' => $this->ftp_data['ftp_path']
            );
            
            case 2: return $this->ftp_data['itemtypes'][$itemtype];
        }
        return false;
    }
    public function connectToFTP($itemtype, $conndata=''){
        if (empty($conndata)) $conndata = $this->dataConnexions($itemtype);
        
        if ($conndata){
            try{
                $this->ftppath = $conndata['ftp_path'];
                $this->ftpport = $conndata['ftp_port'];
                $this->sftp = new \phpseclib\Net\SFTP($conndata['ftp_host'], $conndata['ftp_port']);

                $this->ftpuser = $conndata['ftp_user'];
                $this->ftppwd = $conndata['ftp_pwd'];
                $this->sftp_login = $this->sftp->login($this->ftpuser, $this->ftppwd);

                if ($this->sftp_login) {
                    //create dir if not exists
                    $auxpath = explode('/', $this->ftppath);
                    $path = '';
                    foreach ($auxpath as $folder) {
                        if (!$folder)
                            continue;
                        $path .= $folder . '/';
                        if (!$this->sftp->chdir($folder))
                            $this->sftp->mkdir($folder);
                        $this->sftp->chdir($folder);
                    }
                }
            }catch(\Exception $e){
                throw $e;
            }
        }
        else throw new \Exception('No se pudo conectar al ftp');
        
        return $this->sftp_login;
    }
    
    public function getPath() {
        return $this->path;
    }

    public function getFilePerm($perm, $type, $id) {
        return true;
    }

    /* Import and img form URL */

    public function importImage($url, $item_id, $item_type, $filename = '', $filetype = 'image') {
        if (!@getimagesize($url))
            throw new \Exception('No se encontr� la imagen a importar: ' . $url . ' -> ' . @getimagesize($url));

        if (empty($filename))
            $filename = $this->interfacetools->generateRandomUsername();

        $ext = explode('.', $url);
        $ext = $ext[count($ext) - 1];
        $todest = $this->rootdir . '/..' . $this->path . $item_type;
        if (!file_exists($todest))
            mkdir($todest);
        $todest .= '/' . $item_id;
        if (!file_exists($todest))
            mkdir($todest);


        $todest .= '/' . trim(str_replace(' ', '_', $filename)) . '.' . $ext;

        if (file_exists($todest))
            return '';


        @copy($url, $todest);

        $image = $this->createimage($filename, floatval(filesize($todest)), $item_type, $item_id, trim(str_replace(' ', '_', $filename)) . '.' . $ext, $filetype, $todest.'/');
    }

    /**
     * Upload an image
     * 
     * @param int $item_id The image is associated to an item with this id
     * @param string $item_type It's the type of the item that the image is associated to
     * @return the response of the uploadHandler.php
     * @author Pacolmg
     */
    public function uploadImage($item_id, $item_type, $filetype = 'image', $changefilename = false, $dataimagetype=null, $uploaddir='') {
        if ($this->forbiddenupload) throw new \Exception('No se puede subir la imagen');
            
        require_once($this->rootdir . '/../web/bundles/dataimage/plugins/jquery-file-upload/server/php/UploadHandler.php');

        //upload and save the image in db

        if (empty($uploaddir)) $uploaddir = $this->rootdir . '/..' . $this->path . $item_type . '/' . $item_id . '/';
        else{
            $ftpuploaddir=$uploaddir;
            $uploaddir = $this->rootdir . '/..' . $this->path . $uploaddir;
        }
        $upload_handler = new UploadHandler(
                array(
            'upload_dir' => $uploaddir,
            'upload_url' => $uploaddir
                )
        );

        //get the title from the filename
        $newfilename = $filename = $upload_handler->response['files'][0]->name;
        $aux = explode('.', $filename);
        $ext = $aux[count($aux) - 1];
        unset($aux[count($aux) - 1]);

        //change filename to numeric
        if ($this->changefilename || $changefilename) {
            $newfilename = str_replace('.', '-', microtime(true)) . '.' . $ext;
            rename($uploaddir. $filename, $uploaddir . $newfilename);
        }
        $image = $this->createimage(str_replace('-', ' ', implode(' ', $aux)), floatval(($upload_handler->response['files'][0]->size) / 1000), $item_type, $item_id, $newfilename, $filetype, $uploaddir, $dataimagetype);


        //set the delete url and other params to show
        $upload_handler->response['files'][0]->deleteUrl = $this->router->generate('dataimage_dropimage', array('id' => $image->getId()));
        $upload_handler->response['files'][0]->editUrl = $this->router->generate('dataimage_formimage', array('id' => $image->getId()));
        $upload_handler->response['files'][0]->moveUrl = $this->router->generate('dataimage_formimagemove', array('id' => $image->getId()));
        $upload_handler->response['files'][0]->name = $image->getTitle();
        $upload_handler->response['files'][0]->filetype = $image->getFiletype();
        $upload_handler->response['files'][0]->size = $image->getFilesize();

        list($upload_handler->response['files'][0]->url, $upload_handler->response['files'][0]->thumbnailUrl) = $this->getDataimagePath($item_type, $item_id, $image->getFilename());

        //upload via sftp in case
        if (!empty($this->ftp_data)) {
            $this->connectToFTP($item_type);
            if (!$this->sftp_login && !$this->forbiddenupload) {
                $this->sftp_login = $this->sftp->login($this->ftpuser, $this->ftppwd);
            }
            if ($this->sftp_login) {
                //create dir if not exists
                if (!empty($ftpuploaddir)) $auxpath = explode('/', $ftpuploaddir);
                else $auxpath = array($item_type, $item_id);
                foreach ($auxpath as $folder) {
                    if (!$folder)
                        continue;
                    if (!$this->sftp->chdir($folder))
                        $this->sftp->mkdir($folder);
                    $this->sftp->chdir($folder);
                }
                $this->sftp->put($newfilename, $uploaddir . $newfilename, \phpseclib\Net\SFTP::SOURCE_LOCAL_FILE);
                $this->sftp->chdir('../..');
            }
            unlink($uploaddir. $newfilename);
            @unlink($uploaddir.'thumbnail/'.$newfilename);
        }

        $files = $upload_handler->response;
        if (!isset($files['files'])) return $files;
        
        $files['files'][0]->comments = $image->getComments();
        $files['files'][0]->link = $image->getLink();
        if ($this->dataimagetypechoices){
            $function = $this->dataimagetypechoicesfunction;
            if ($this->container->has($this->dataimagetypechoices)) $files['files'][0]->dataimagetypename = $image->getDataimagetype() ? $this->container->get($this->dataimagetypechoices)->$function($image->getDataimagetype()):'-';
            else $files['files'][0]->dataimagetypename = $image->getDataimagetype() ? $this->dataimagetypechoices->$function($image->getDataimagetype()):'-';
        }
        $files['files'][0]->dataimagetype = $image->getDataimagetype();
        $files['files'][0]->id = $image->getId();
        return $files;
    }

    protected function createimage($title, $size, $item_type, $item_id, $filename, $filetype, $uploaddir, $dataimagetype=null) {
        $image = new Dataimage();

        $image->setTitle($title);
        $image->setFilesize($size);

        $finfo = new \finfo();
        $image->setMimetype($finfo->file($uploaddir . $filename));

        $image->setFiletype($filetype);

        $image->setComments('');
        $image->setNumorder($this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($item_type, $item_id, $filetype) + 1);
        $image->setFilename($filename);
        $image->setItemid($item_id);
        $image->setItemtype($item_type);
        
        if ($dataimagetype){
            $image->setDataimagetype($dataimagetype);
        }

        $this->em->persist($image);
        $this->em->flush();

        return $image;
    }

    /**
     * Change the physical file from one item to another
     * 
     */
    public function changeDataimageitem($image, $olditemid) {
        if (!file_exists($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/'))
            mkdir($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/');
        rename($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $olditemid . '/' . $image->getFilename(), $this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $image->getItemid() . '/' . $image->getFilename());
        if (!file_exists($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $image->getItemid() . '/thumbnail/'))
            mkdir($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $image->getItemid() . '/thumbnail/');
        @rename($this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $olditemid . '/thumbnail/' . $image->getFilename(), $this->rootdir . '/..' . $this->path . $image->getItemtype() . '/' . $image->getItemid() . '/thumbnail/' . $image->getFilename());
        $this->em->flush();
        $this->reorderImages($olditemid, $image->getItemtype(), $image->getFiletype());
    }

    /**
     * Get an image
     * 
     * @param int $id
     * @return Dataimage
     */
    public function getImage($id) {
        return $this->em->getRepository('DataimageBundle:Dataimage')->findOneById($id);
    }

    /**
     * Get the images
     * 
     * @param int $item_id The images are associated to an item with this id
     * @param string $item_type It's the type of the item that the images are associated to
     * @return an array with the images
     * @author Pacolmg
     */
    public function getImages($item_id, $item_type, $array_mode = 1, $filetype = 'image', $strict = false) {
        $data = array();

        $files = $this->em->getRepository('DataimageBundle:Dataimage')->findByItem($item_id, $item_type, $filetype, $strict);
        if (!$array_mode)
            return $files;

        foreach ($files as $k => $file) {
            list($data[$k]['url'], $data[$k]['thumbnailUrl']) = $this->getDataimagePath($file->getItemtype(), $file->getItemid(), $file->getFilename());
            $data[$k]['id'] = $file->getId();
            $data[$k]['filetype'] = $filetype;
            $data[$k]['name'] = $file->getTitle();
            $data[$k]['subtitle'] = $file->getSubtitle();
            $data[$k]['comments'] = $file->getComments();
            $data[$k]['link'] = $file->getLink();
            if ($this->dataimagetypechoices){
                $function = $this->dataimagetypechoicesfunction;
                if ($this->container->has($this->dataimagetypechoices)) $data[$k]['dataimagetypename'] = $file->getDataimagetype() ? $this->container->get($this->dataimagetypechoices)->$function($file->getDataimagetype()):'-';
                else $data[$k]['dataimagetypename'] = $file->getDataimagetype() ? $this->dataimagetypechoices->$function($file->getDataimagetype()):'-';
            }
            $data[$k]['dataimagetype'] = $file->getDataimagetype();
            $data[$k]['deleteUrl'] = $this->router->generate('dataimage_dropimage', array('id' => $file->getId()));
            $data[$k]['editUrl'] = $this->router->generate('dataimage_formimage', array('id' => $file->getId()));
            $data[$k]['moveUrl'] = $this->router->generate('dataimage_formimagemove', array('id' => $file->getId()));
            $data[$k]['size'] = $file->getFilesize();
            $data[$k]['item_id'] = $file->getItemid();
            $data[$k]['item_type'] = $file->getItemtype();
            $data[$k]['deleteType'] = 'DELETE';
        }


        return array('errorCode' => 0, 'files' => $data);
    }

    public function getDocuments($item_id, $item_type, $array_mode = 1) {
        return $this->getImages($item_id, $item_type, $array_mode, 'document');
    }

    /**
     * Drop an Image
     * 
     * @param int $id The id of the image to delete
     * @return the response of the uploadHandler.php
     * @author Pacolmg
     */
    public function dropImage($id, $deleteurl='') {
        error_reporting(E_ALL | E_STRICT);
        require_once($this->rootdir . '/../web/bundles/dataimage/plugins/jquery-file-upload/server/php/UploadHandler.php');

        $image = $this->em->getRepository('DataimageBundle:Dataimage')->findOneById($id);
        if (!$image)
            return array("error" => "No se encontr� la imagen");

        $_GET['file'] = $image->getFilename();
        if (empty($deleteurl)) $deleteurl = $image->getItemtype() . '/' . $image->getItemid() . '/';
        
        if (!empty($this->ftp_data)) {
            $this->connectToFTP($image->getItemType());
            if ($this->sftp_login) {
                //throw new \Exception($this->ftppath . '/' . $deleteurl .'_____'.$_GET['file']);
                $this->sftp->delete($this->ftppath . '/' . $deleteurl .$_GET['file']);
                //$this->sftp->rename($this->ftppath . '/' . $deleteurl .$_GET['file'], $this->ftppath . '/' . $deleteurl .'_____'.$_GET['file']);
            }
        } else {
            
            $upload_handler = new UploadHandler(
                    array(
                'upload_dir' => $this->rootdir . '/..' . $this->path . $deleteurl,
                'upload_url' => $this->rootdir . '/..' . $this->path . $deleteurl,
                '_method' => 'DELETE'
                    )
            );
        }

        if (!isset($upload_handler->response['error'])) {
            $this->em->remove($image);
            $this->em->flush();
        } else
            return $upload_handler->response;

        //reorder the images of the item
        return $this->reorderImages($image->getItemid(), $image->getItemtype(), $image->getFiletype());
    }

    /**
     * Get the path of a image
     * 
     * @param int $id It's the id of the item that the image is associated to
     * @param string $type It's the type of the item that the image is associated to
     * @param string $filename It's the name that the image is saved in disk
     * @return array[0] the url of the image, and array[1] the url of the thumbnail of the image 
     * @author Pacolmg
     */
    public function getDataimagePath($type, $id, $filename, $withroot = false) {
        if ($withroot)
            return array($this->rootdir . '/..' . $this->path . $type . '/' . $id . '/' . $filename, $this->rootdir . '/..' . $this->path . $type . '/' . $id . '/thumbnail/' . $filename);

        return array($this->router->generate('dataimage_public_getfile', array('type' => $type, 'id' => $id, 'filename' => $filename)), $this->router->generate('dataimage_public_getfile', array('type' => $type, 'id' => $id, 'filename' => $filename, 'thumbnail' => 'thumbnail')));
    }

    /**
     * Order of the images of an item, from 1 to n
     * 
     * @param int $item_id The images are associated to an item with this id
     * @param string $item_type It's the type of the item that the images are associated to
     * @return true/false
     * @author Pacolmg
     */
    public function reorderImages($item_id, $item_type, $filetype) {
        return $this->interfacetools->reorderItemElems($this->getImages($item_id, $item_type, 0, $filetype));
    }

    /**
     * Move an image from its old position to a new position
     * 
     * @param int $item_id The image is associated to an item with this id
     * @param string $item_type It's the type of the item that the image is associated to
     * @return the response of the uploadHandler.php
     * @author Pacolmg
     */
    public function moveImage($id, $newposition, $filetype = '') {
        $image = $this->getImage($id);
        if (empty($filetype))
            $filetype = $image->getFiletype();
        return $this->interfacetools->moveItemElem('DataimageBundle:Dataimage', $id, $newposition, '', $this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($image->getItemtype(), $image->getItemid(), $filetype, true), '', '', $filetype);
    }

    public function findTotalElems($filetype = '') {
        return $this->em->getRepository('DataimageBundle:Dataimage')->findTotalElems($filetype);
    }

    public function findMaxNumOrderByElem($type, $id_elem, $filetype) {
        return $this->em->getRepository('DataimageBundle:Dataimage')->findMaxNumOrderByElem($type, $id_elem, $filetype);
    }

    public function modifyImage() {
        $this->em->flush();
    }

    /* Devuelve una imagen en formato objeto */

    public function getDataimageObject($id) {
        return $this->em->getRepository('DataimageBundle:Dataimage')->findOneById($id);
    }

    /* Devuelve una imagen en formato objeto por nombre de fichero */

    public function getDataimageObjectByFilename($type, $id_elem, $filename) {
        $docs = $this->em->getRepository('DataimageBundle:Dataimage')->findBy(array('itemtype'=>$type, 'itemid'=>$id_elem, 'filename'=>$filename));
        if (isset($docs[0])) return $docs[0];
        
        return '';
    }
    
    public function getDataimageByDataimagetype($dataimagetype){
        return $this->em->getRepository('DataimageBundle:Dataimage')->findBy(array('dataimagetype'=>$dataimagetype));
    }

}
