<?php

namespace Greetik\DataimageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends Controller {

    public function getfileAction($type, $id, $filename, $thumbnail = '') {
        ini_set('zlib.output_compression', 'Off');
        ini_set('output_buffering', 'Off');
        ini_set('output_handler', '');
        //@apache_setenv('no-gzip', 1);
        
        if (!$this->get($this->getParameter('dataimage.uploadservice'))->getFilePerm('publicview', $type, $id))
            throw new \Exception('No tiene permiso para ver el fichero');

        $dataimage = $this->get('dataimage.tools')->getDataimageObjectByFilename($type, $id, $filename);
        if (!$dataimage) throw new \Exception('El fichero no existe');
        
        try{
            if (method_exists($this->get($this->getParameter('dataimage.uploadservice')), 'getUploadDir'))
                $filepath = $this->get($this->getParameter('dataimage.uploadservice'))->getUploadDir($type, $id, true);
            else $filepath='';
        } catch (\Exception $e){
            $filepath='';
        }
        if (!$filepath) $filepath = $this->get('dataimage.tools')->getPath() . $type . '/' . $id . '/';

        
        if (!$this->getParameter('dataimage.ftp_data'))
            return new BinaryFileResponse($this->getParameter('kernel.root_dir') . '/..' . $filepath . (($thumbnail == 'thumbnail') ? 'thumbnail/' : '') . $filename);

        $connexception=new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No se encontró el fichero');
        $i=0;
        do{
            $i++;
            try{
                $conndata = $this->get($this->getParameter('dataimage.uploadservice'))->dataConnexions($type, true);
                $sftp = new \phpseclib\Net\SFTP($conndata['ftp_host'], $conndata['ftp_port']);
                $sftp_login = $sftp->login($conndata['ftp_user'], $conndata['ftp_pwd']);
                $connected=true;
            }catch(\Exception $e){
                sleep(5);
                $connected=false;
                $connexception=$e;
            }
        }while(!$connected && $i<5);
        
        if (isset($sftp_login)) {
            try {
                $response = $this->bringFromFTP($sftp, $type, $id, $filename, $filepath, $dataimage->getTitle());
                return $response;
            } catch (\Exception $e) {
                sleep(5);
                try {
                    $response = $this->bringFromFTP($sftp, $type, $id, $filename, $filepath, $dataimage->getTitle());
                    return $response;
                } catch (\Exception $e) {
                    throw $e;
                    throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No se encontró el fichero.');
                }
            }

            
        }else{
            throw $connexception;
        }

        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('No se encontró el fichero.');
    }

    protected function bringFromFTP($sftp, $type, $id, $filename, $filepath='', $title='') {
        if (empty($filepath)) $filepath = $type . '/' . $id . '/';
        
        //throw new \Exception($this->getParameter('dataimage.ftp_path') . '/' . $filepath . $filename);
        $response = new Response($sftp->get( '/' . $filepath . $filename), 200);
        $response->headers->set('Content-Type', 'application/octet-stream');
        if ($title){
            $ext = explode('.', $filename);
            $ext = $ext[count($ext)-1];
            $filename = $this->get('beinterface.tools')->spaceencodeurl($title).'.'.$ext;
        }
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        return $response;
    }

}