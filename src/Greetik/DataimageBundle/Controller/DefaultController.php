<?php

namespace Greetik\DataimageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UploadHandler;
use Greetik\DataimageBundle\Entity\Dataimage;
use Greetik\DataimageBundle\Form\Type\DataimageType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function indexAction($type, $id, $filetype = 'image') {
        //use the interface
        return $this->render('DataimageBundle:Dataimage:index.html.twig', array(
                    'configFiles' => array('modifyAllow' => true, 'id' => $id, 'type' => $type, 'filetype' => $filetype)
        ));
    }

    /**
     * If the method is POST, upload a new image. If it's GET get all the images from the id pass by parameter
     * 
     * @param int $id is received by Get Request and it's the id of the item that the image is associated to
     * @param string $type is received by Get Request and it's the type of the item that the image is associated to
     * @return the data of the uploaded image, or a images array
     * @author Pacolmg
     */
    public function uploadAction(Request $request, $type, $id, $filetype = 'image') {
        if ($request->getMethod() == "POST") {
            try {
                return new Response(json_encode($this->get($this->getParameter('dataimage.uploadservice'))->uploadImage($id, $type, $filetype, $this->getParameter('dataimage.changefilename'))), 200, array('Content-Type' => 'application/json'));
            } catch (\Exception $e) {
                return new Response(json_encode(array('files' => array(array('error' => $e->getMessage()/*.' -> '.$e->getLine().' -> '.$e->getFile()*/)))), 200, array('Content-Type' => 'application/json'));
            }
        } else {
            if ($request->getMethod() == "GET")
                return new Response(json_encode($this->get($this->getParameter('dataimage.uploadservice'))->getImages($id, $type, 1, $filetype)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('files' => array())), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Delete an image
     * 
     * @param int $id It's the id of the image
     * @author Pacolmg
     */
    public function dropAction(Request $request) {
        if ($request->getMethod() != "POST")
            return new Response(json_encode(array('error' => 'No puede hacer esta operación')), 200, array('Content-Type' => 'application/json'));
        if (!$request->get('id'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No se encontró la imagen')), 200, array('Content-Type' => 'application/json'));
        return new Response(json_encode($this->get($this->getParameter('dataimage.uploadservice'))->dropImage($request->get('id'))), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Show the edit image form
     * 
     * @author Pacolmg
     */
    public function modifyformAction(Request $request) {

        if ($request->get('id'))
            $image = $this->get('dataimage.tools')->getDataimageObject($request->get('id'));
        else
            $image = new Dataimage();

        if ($this->container->has($this->getParameter('dataimage.imagetypechoices'))) $choices = $this->get($this->getParameter('dataimage.imagetypechoices'))::getChoices();
        else $choices = $this->getParameter('dataimage.imagetypechoices')::getChoices();
        $newForm = $this->createForm(DataimageType::class, $image, array('imagetypes' => $choices));

        return $this->render('DataimageBundle:Dataimage:insert.html.twig', array('new_form' => $newForm->createView(), 'id' => $request->get('id'), 'descriptionfield' => $this->getParameter('dataimage.descriptionfield'), 'subtitlefield' => $this->getParameter('dataimage.subtitlefield'), 'imagetypefield' => $this->getParameter('dataimage.imagetypefield'), 'linkfield' => $image->getFiletype() == 'image' && $this->getParameter('dataimage.linkfield')));
    }

    /**
     * Edit the data of image
     * 
     * @param int $id is received by Get Request
     * @param Image $item is received by Post Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request) {
        $item = $request->get('dataimage');
        if (@$item['id'] !== null) {
            $image = $image = $this->get('dataimage.tools')->getDataimageObject($item['id']);
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No se encontró la imagen a modificar')), 200, array('Content-Type' => 'application/json'));

        if ($this->container->has($this->getParameter('dataimage.imagetypechoices'))) $choices = $this->get($this->getParameter('dataimage.imagetypechoices'))::getChoices();
        else $choices = $this->getParameter('dataimage.imagetypechoices')::getChoices();
        $editForm = $this->createForm(DataimageType::class, $image, array('imagetypes' => $choices));

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->get($this->getParameter('dataimage.uploadservice'))->modifyImage($item['id']);
            return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
        } else {
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * move the image
     * 
     * @param int $id is received by Get Request
     * @param Image $item is received by Post Request
     * @author Pacolmg
     */
    public function moveAction(Request $request) {
        if (!$request->get('id'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No se encontró la imagen')), 200, array('Content-Type' => 'application/json'));
        return new Response(json_encode($this->get($this->getParameter('dataimage.uploadservice'))->moveImage($request->get('id'), $request->get('newposition'))), 200, array('Content-Type' => 'application/json'));
    }

}
