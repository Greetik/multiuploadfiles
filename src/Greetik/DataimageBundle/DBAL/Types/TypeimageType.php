<?php
namespace Greetik\DataimageBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class TypeimageType extends AbstractEnumType
{
    const TIPO1 = 1;
    const TIPO2 = 2;
    
    protected static $choices = [
        self::TIPO1 => 'Tipo 1',
        self::TIPO2 => 'Tipo 2'
    ];
    
}