<?php

namespace Greetik\DataimageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataimageType
 *
 * @author Paco
 */
class DataimageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('title')
            ->add('subtitle')
            ->add('comments')
            ->add('link', TextType::class)
            ->add('id', HiddenType::class, array("mapped" => false));

        if (count($options['_items']) > 0) {
            $builder->add('item_id', 'choice', array('required' => false, 'empty_data' => null, 'choices' => $options['_items']));
        }

        if (!empty($options['imagetypes'])) {
            $builder->add('dataimagetype', ChoiceType::class, array('choices' => $options['imagetypes']));
        }

    }

    public function getName()
    {
        return 'Dataimage';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\DataimageBundle\Entity\Dataimage',
            '_items' => array(),
            'imagetypes' => ''
        ));
    }
}

