<?php

namespace Greetik\DataimageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dataimage');

        
        $rootNode
            ->children()
                ->booleanNode('subtitlefield')->defaultValue(false)->end()
                ->booleanNode('descriptionfield')->defaultValue(true)->end()
                ->booleanNode('linkfield')->defaultValue(true)->end()
                ->booleanNode('imagetypefield')->defaultValue(false)->end()
                ->scalarNode('imagetypechoices')->defaultValue('Greetik\DataimageBundle\DBAL\Types\TypeimageType')->end()
                ->scalarNode('imagetypechoicesfunction')->defaultValue('getReadableValue')->end()
                ->scalarNode('uploadservice')->defaultValue('dataimage.tools')->end()
                ->scalarNode('path')->defaultValue('web/uploads')->end()
                ->scalarNode('changefilename')->defaultValue(false)->end()
                ->scalarNode('ftp_data')->defaultValue('')->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
