# README #


### What is this repository for? ###

* Multiuploadfiles is a bundle for symfony3 to multiupload documents and images and attached it to an item, also save the data asociated to the file into the db so you can change the title and the description of it.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\DataimageBundle\DataimageBundle()


##In the config.yml you can add the service##
dataimage:
    uploadservice: app.dataimage